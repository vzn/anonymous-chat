#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import uuid
import json
import tornado.ioloop
import tornado.web
import tornado.websocket


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('index.html')


class ChatHandler(tornado.websocket.WebSocketHandler):
    # 用户存储当前聊天室用户
    waiters = set() # 每个人的socket对象# self
    # 用于存储历时消息
    messages = []

    def open(self):
        """
        客户端连接成功时，自动执行
        :return: 
        """
        ChatHandler.waiters.add(self)
        uid = str(uuid.uuid4())
        self.write_message(uid)

        for msg in ChatHandler.messages:
            content = self.render_string('message.html', **msg)
            self.write_message(content)

    def on_message(self, message):
        """
        客户端连发送消息时，自动执行
        :param message: 
        :return: 
        """
        msg = json.loads(message)   # JSON load 回来
        ChatHandler.messages.append(msg)    # 添加到历史消息里面

        for client in ChatHandler.waiters:
            content = client.render_string('message.html', **msg)
            client.write_message(content)

    def on_close(self):
        """
        客户端关闭连接时，，自动执行
        :return: 
        """
        ChatHandler.waiters.remove(self)


def run():
    # 设置
    settings = {
        'template_path': 'templates', # 模板文件目录
        'static_path': 'static',    # 静态文件目录
    }
    # 路由
    application = tornado.web.Application([
        (r"/", IndexHandler),
        (r"/chat", ChatHandler),
    ], **settings)
    # 监听端口
    application.listen(8888, '0.0.0.0')
    # 启动
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    run()